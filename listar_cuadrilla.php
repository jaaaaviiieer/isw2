<?php 
    include 'conexion.php';
    $query="SELECT * FROM cuadrillas";
    $consulta_cuadrillas = $conexion->query($query);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listar cuadrillas</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
    
</head>
<body>
    <div class="contenedor">
        <div class="table-responsive" style="padding: 1%">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Eliminar/Modificar</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($consulta_cuadrillas->num_rows >0){
                            while($lb = $consulta_cuadrillas->fetch_assoc()){
                    ?>
                    <tr>
                        <td><?php echo $lb['id'] ?></td>
                        <td><?php echo $lb['nombre_cuadrilla'] ?></td>
                        <td>
                            <a type ="Eliminar" class="btn btn-danger m-r-1em" href=<?php echo "eliminar_cuadrilla.php?id=" . $lb['id']?>>Eliminar</a>
                            <a type ="Editar" class='btn btn-warning m-r-1em' href=<?php echo "editar_cuadrilla.php?id=" . $lb['id']?>>Editar</a>
                        </td>
                    </tr>
                    
                    <?php }} ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="botones">
        <a href="cuadrillas.php"> Agregar cuadrilla </a>
    </div>

</body>
</html>