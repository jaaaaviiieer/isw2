<?php 
    include 'conexion.php';
    $rut = $_GET['rut'];
    $query="SELECT * FROM trabajadores WHERE rut='$rut'";
    $consulta_trabajadores = $conexion->query($query);

?>
<?php
    if($consulta_trabajadores->num_rows >0){
        $lb = $consulta_trabajadores->fetch_assoc();}
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Agregar trabajador</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet" href="css/styles.css">
</head>
<body>
	
	<div class="container">
		<form action="<?php echo "edit.php?rut=" . $lb['rut']?>" method="POST">	
			<div class="row justify-content-center align-items-center vh-100">
				<div class="d-flex h-75">
					<div class="col-3 imagen">
					</div>
					
						<div class="col-9 formulario">
							<h2 class="text-center">Registro de trabajadores</h2>
							<h6 class="text-center">Este formulario permite registrar trabajadores al sistema</h4>
							<form action="">
								<div class="container">
									<div class="row row-cols-2">
										<div class="col form-group">
											<label for=""><b>Nombre</b></label>
											<input type="text" class="form-control" name="nombre" value="<?php echo $lb['nombre']?>" required>
										</div>
										<div class="col form-group">
											<label for=""><b>Apellidos</b></label>
											<input type="text" class="form-control" name="apellidos" value="<?php echo $lb['apellidos']?>"required>
										</div>
										<div class="col form-group">
											<label for=""><b>RUT</b></label>
											<input type="text" class="form-control" name="rut" value="<?php echo $lb['rut']?>"  pattern="[0-9]{8}[-]{1}[0-9-k]" title="El campo rut debe ir sin puntos y guión" required>
										</div>
										
										<div class="col form-group">
											<label for=""><b>Correo Electrónico</b></label>
											<input type="email" class="form-control" name="correo" value="<?php echo $lb['correo']?>" required>
										</div>
										<div class="col form-group">
											<label for=""><b>Teléfono</b></label>
											<input type="number" class="form-control" name="telefono" value="<?php echo $lb['telefono']?>" required>
										</div>
										<div class="col form-group">
											<label for=""><b>Documentos del trabajador</b></label>
											<input type="file" class="form-control" name="archivo[]" multiple >
										</div>
									</div>
									<button type="submit" class="btn btn-secondary btn-lg btn-block">Guardar Cambios</button>

								</div>
							</form>
							<br>
						</div>
					
				</div>
			</div>
		</form>
	</div>

	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>