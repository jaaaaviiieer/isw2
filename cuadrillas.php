<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Agregar proyecto</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet" href="css/styles.css">
</head>
<body>
	
	<div class="container">
			
			<div class="row justify-content-center align-items-center vh-100">
				<div class="d-flex h-75 w-75">
					<div class="col-3 imagen3">
					</div>
					
						<div class="col-9 formulario">
							<h2 class="text-center">Registro de cuadrillas</h2>
							<h6 class="text-center">Este formulario permite registrar cuadrillas al sistema</h4>
							<form action="agregar_cuadrilla.php" method="POST" multipart="" enctype="multipart/form-data">
								<div class="container">
									<div class="row row-cols-9">
										<div class="col form-group">
											<label for=""><b>Nombre cuadrilla</b></label>
											<input type="int" class="form-control" name="nombre_cuadrilla" placeholder="Ingrese el nombre de la cuadrilla" required>
										</div>
																			
									</div>
									<button type="submit" class="btn btn-secondary btn-lg btn-block">Registrar cuadrilla</button>

								</div>
							</form>
							<br>
							<form action="cuadrillas.php" >
								<div class="col form-group">
									<button type="submit" class="btn btn-secondary btn-lg btn-block">Listar cuadrillas</button>
								</div>
							</form>


						</div>
					
				</div>
			</div>
		
	</div>

	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>